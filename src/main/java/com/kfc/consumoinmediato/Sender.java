package com.kfc.consumoinmediato;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeoutException;

public class Sender {
    private final static String QUEUE_NAME = "hello";
    public static void main(String... args){
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try(
                Connection connection = factory.newConnection();
                Channel channel = ((Connection) connection).createChannel()){
           //Conectarse a una cola o crear una nueva
                channel.queueDeclare(QUEUE_NAME,false,false,false,null);
            for( int i=0;i<1000;i++){
                String message="Nuevo mensaje para la cola"+new Date();
                 /*
            Parametros:
                - Si la cola es temporal (transiente)
                - Características:
                    - Que cada mensaje sea transiente o permanente
                - Mensaje guardado en bytes
            * */
                channel.basicPublish("",QUEUE_NAME,null,message.getBytes("UTF-8"));
                System.out.println("[XX] Mensaje Enviado: '" + message+ "'");
            }

        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
