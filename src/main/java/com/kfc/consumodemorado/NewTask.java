package com.kfc.consumodemorado;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.IOException;
import java.util.Date;
import java.util.concurrent.TimeoutException;

public class NewTask {
    private final static String QUEUE_NAME = "task_queue";
    public static void main(String... argv){
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        try(
                Connection connection = factory.newConnection();
                Channel channel = ((Connection) connection).createChannel()){
                channel.queueDeclare(QUEUE_NAME,true,false,false,null);

                String message= String.join(" ",argv);
            for( int i=0;i<20;i++) {
                channel.basicPublish("", QUEUE_NAME,
                        MessageProperties.PERSISTENT_TEXT_PLAIN,
                        message.getBytes("UTF-8"));
                System.out.println("[XX] Mensaje Enviado: '" + message + "'");
            }

        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
