package com.kfc.clientewstemperatura;

import com.kfc.clientewstemperatura.Temperature;

import javax.ws.rs.client.*;
import javax.ws.rs.core.Response;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

public class TemperatureServiceClient {
    public static void main(String... args) throws ExecutionException, InterruptedException {
        Client client = ClientBuilder.newClient();
        WebTarget target = client.target("http://localhost:8080/weather-service-1.0-SNAPSHOT/weather/temperature/Quevedo");

        Invocation.Builder builder = target.request();

        //Metodo1
//        Temperature res = builder.get(Temperature.class);
        //System.out.println("T: " + res.getTemp() + " en " + res.getCiudad());

        //Metodo 2
        //Future<Temperature> futureResult = builder.async().get(Temperature.class);
        //Temperature res = futureResult.get();
        //System.out.println("T: " + res.getTemp() + " en " + res.getCiudad());

/*
        //Metodo 3
        builder.async().get(
                new InvocationCallback<Temperature>() {

                    public void completed(Temperature res){
                        System.out.println("T: " + res.getTemp() + " en " + res.getCiudad());
                    }

                    public void failed(Throwable throwable){

                    }
                }
        );
        */

        //Con Lambdas
        CompletionStage<Response> response = builder.rx().get();
        response.thenAccept(res ->{
            Temperature t = res.readEntity(Temperature.class);
            System.out.println("T: " + t.getTemp() + " en " + t.getCiudad());
        });
        client.close();
    }
}