package com.kfc.clientewstemperatura;
public class Temperature {
    private String temp;
    private String ciudad;

    public Temperature(){}
    public Temperature(String temp,String ciudad) {
        this.temp = temp;
        this.ciudad = ciudad;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}

